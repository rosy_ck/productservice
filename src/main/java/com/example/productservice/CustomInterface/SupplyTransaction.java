package com.example.productservice.CustomInterface;

import com.example.productservice.Entities.Supply;
import com.example.productservice.Repositories.SupplyRepository;
import com.example.productservice.Repositories.SupplyRepositoryCustom;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class SupplyTransaction implements SupplyRepositoryCustom {
    private SupplyRepository supplyRepository;


    public SupplyTransaction(SupplyRepository supplyRepository) {
        this.supplyRepository = supplyRepository;
    }

    @Override
    public boolean SaveSupplyAfterOrder(
            Long supplyId,Double quantity) {
        System.out.println("CIAO CIAO");
        Optional<Supply> sup = supplyRepository.findById(supplyId) ;
        if(!sup.isEmpty()){
            Supply su = sup.get();
            su.setQuantity(quantity);
                supplyRepository.save(su);
               return true;
            }else{
                return false;
            }
    }
}
