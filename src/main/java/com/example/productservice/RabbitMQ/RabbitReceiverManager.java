package com.example.productservice.RabbitMQ;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitReceiverManager {
    //nome dell'exchange (posso averne più di uno in un server rabbit)
    public static final String topicExchangeName = "tofoodExchange";

    static final String queueName = "supplyquantityMQ"; //nome della coda da qui passano i messagi, all'interno dell exchange

    //qua viene creata effettivamente la coda
    // durable:true i messaggi non vengono eliminati,se non vengono consumati
    @Bean
    Queue queue() {
        return new Queue(queueName, true);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(topicExchangeName);
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        //la routing key serve per riconoscere dove instradare il messaggio
        return BindingBuilder.bind(queue).to(exchange).with("supply.qt.#");
    }

    @Bean
    public static ConnectionFactory connectionFactory() {
        //qua ci colleghiamo alla coda
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory("host.docker.internal");
        //CachingConnectionFactory connectionFactory = new CachingConnectionFactory("localhost");
         connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        System.out.println("Manager Sender: " + connectionFactory.getUsername());
        return connectionFactory;//stringa di connessione
    }

    @Bean
    public ObjectMapper objectMapper() { return new ObjectMapper(); }

    @Bean
    public MessageConverter jsonMessageConverter() {
        // return new ObjectMapper().convertValue(objectNode.get("Shop"), Shop.class);
        return new Jackson2JsonMessageConverter(objectMapper());
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        System.out.println("Rabbit reader");
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }
    //container e ilstener servono per definire un Listener che resta sempre in ascolto
// per vedere se ci sono nuovi emssaggi
//dentro la coda
    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                             MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        //listenerAdapter.setMessageConverter(jsonMessageConverter());
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(Receiver receiver) {
//        QueueingConsumer consumer = new QueueingConsumer(channel);
//        channel.basicConsume(QUEUE_NAME, true, consumer);
//        while (true) {
//            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
//            String message = new String(delivery.getBody());
//            System.out.println(" [x] Received '" + message + "'");
//        }
        //metodo che permette la lettura continua dei messaggi
        return new MessageListenerAdapter(receiver, "readMessage");
    }
}
