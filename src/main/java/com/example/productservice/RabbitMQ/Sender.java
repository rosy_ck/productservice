package com.example.productservice.RabbitMQ;


import com.example.productservice.ServiceRunner;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

//implements CommandLineRunner
@Component
public class Sender  {
    //sender della rabbitMQ
    private final RabbitTemplate rabbitTemplate;
    //private final Receiver receiver;

    //Receiver receiver, RabbitTemplate rabbitTemplate
    public Sender() {
        //this.receiver = new Receiver();
        //in questo punto sender si connettte alla connection factory adatta
        this.rabbitTemplate = new RabbitTemplate(RabbitSenderManager.connectionFactory());
    }

    //@Override
    //public void run(String... args) throws Exception {
        //System.out.println("It's awake...");
        //rabbitTemplate.convertAndSend(ServiceRunner.topicExchangeName, "prova.baz", "Hello from RabbitMQ (by Martina)!");
        //receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
    //}

    public int sendingMessage(String jsonMessage) {
        try {
            rabbitTemplate.convertAndSend(RabbitSenderManager.topicExchangeName, "order.#", jsonMessage);
            //receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
            System.out.println("Msg mandato");
            return 0;
        } catch(Exception ex){
            return -1;
        }
    }
}
