package com.example.productservice.RabbitMQ;

import com.example.productservice.Entities.Supply;
import com.example.productservice.Repositories.SupplyRepository;
import com.example.productservice.Repositories.SupplyRepositoryCustom;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

@Component
public class Receiver {
    private final SupplyRepositoryCustom supCutom;
    //private static final Logger logger = LoggerFactory.getLogger(Reader.class);
    private SupplyRepository supplyRepository;
    private CountDownLatch latch = new CountDownLatch(1);
    private Sender send;
    private ReentrantLock lock = new ReentrantLock();
    Receiver(SupplyRepository supplyRepository, Sender s, SupplyRepositoryCustom supCutom){
       this.send = s;
       this.supplyRepository = supplyRepository;
       this.supCutom=supCutom;
    }


    public void readMessage(String message) throws Exception {

        System.out.println("Received by Object Receiver <" + message + ">");
        ObjectMapper om = new ObjectMapper();
        JsonNode items = om.readTree(message);
        Long supplyId= items.get("id").asLong();
        Double quantity= items.get("quantity").asDouble();
        supCutom.SaveSupplyAfterOrder(supplyId,quantity);
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

}

