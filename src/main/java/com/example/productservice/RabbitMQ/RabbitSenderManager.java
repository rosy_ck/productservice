package com.example.productservice.RabbitMQ;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.context.annotation.Bean;


public class RabbitSenderManager {
    public static final String topicExchangeName = "tofoodExchange";

    static final String queueName = "orderMQ"; //nome della coda da qui passano i messagi, all'interno dell exchange

    //qua viene creata effettivamente la coda
    // durable:true i messaggi non vengono eliminati,se non vengono consumati
    @Bean
    Queue queue() {
        return new Queue(queueName, true);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(topicExchangeName);
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        //la routing key serve per riconoscere dove instradare il messaggio
        return BindingBuilder.bind(queue).to(exchange).with("order.#");
    }

    @Bean
    public static ConnectionFactory connectionFactory() {
        //qua ci colleghiamo alla coda
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory("host.docker.internal");
        //CachingConnectionFactory connectionFactory = new CachingConnectionFactory("localhost");
      connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        System.out.println(connectionFactory.getUsername());
        return connectionFactory;//stringa di connessione
    }

    //nome dell'exchange (posso averne più di uno in un server rabbit)

//    @Bean
//    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
//                                             MessageListenerAdapter listenerAdapter) {
//        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
//        container.setConnectionFactory(connectionFactory());
//        container.setQueueNames(queueName);
//        container.setMessageListener(listenerAdapter);
//        return container;
//    }
//
//    @Bean
//    MessageListenerAdapter listenerAdapter(Receiver receiver) {
//        return new MessageListenerAdapter(receiver, "receiveMessage");
//    }
}
