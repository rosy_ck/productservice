package com.example.productservice.Entities;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Supply {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private Long shopId;
    //@NotNull
    //private Long productId;
    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    private Product product;
    //quantità al kg
    @NotNull
    private Double quantity;
    //prezzo al kg
    @NotNull
    private Double price;

    @Override
    public String toString() {
        return "Supply{" +
                "id=" + id +
                ", shopId=" + shopId +
                //", productId=" + productId +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
}
