package com.example.productservice.Controllers;

import com.example.productservice.Entities.Product;
import com.example.productservice.RabbitMQ.Sender;
import com.example.productservice.Repositories.ProductRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1/products")
public class ProductController {
    private ProductRepository productRepository;

    @Autowired
    public ProductController(ProductRepository productRepository) {

        this.productRepository = productRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Product> findAllProduct(){
        return productRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addProduct(@RequestBody Product product) {
        productRepository.save(product);

    }

    @RequestMapping(value = "/category", method = RequestMethod.GET, params = "category")
    public List<Product> findProductByCategory(@RequestParam("category") String category){
        return productRepository.findByCategory(category);
    }
}
