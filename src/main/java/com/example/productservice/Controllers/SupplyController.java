package com.example.productservice.Controllers;

import com.example.productservice.Entities.Supply;
import com.example.productservice.RabbitMQ.Sender;
import com.example.productservice.Repositories.SupplyRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1/supply")
public class SupplyController {
    // private final SupplyTransaction supTran;
    private SupplyRepository supplyRepository;
    private Sender send;

    @Autowired
    public SupplyController(SupplyRepository supplyRepository, Sender s) {
        this.send = s;
        this.supplyRepository = supplyRepository;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public void addSupply(@RequestBody Supply supply) {
        System.out.println("Begin - " + supply.toString());
        try {
            supply = supplyRepository.save(supply);
            //Creating the ObjectMapper object
            ObjectMapper mapper = new ObjectMapper();
            //Converting the Object to JSONString
            String jsonString = mapper.writeValueAsString(supply);
            System.out.println(jsonString);
            send.sendingMessage(jsonString);
        }catch(JsonProcessingException jpex){
            System.out.println(jpex.getStackTrace());
        }
    }

    @RequestMapping(value = "/{shopid}")
    @ResponseStatus(HttpStatus.OK)
    public List<Supply> findSupplyByShop(@PathVariable("shopid") Long shopid){
        System.out.println("Shop:" + shopid);
        return supplyRepository.findByShop(shopid);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update")
    @ResponseStatus(HttpStatus.OK)
    public Supply updateSupply(@RequestBody Supply supply) { //void
        System.out.println(supply.toString());
        Supply SupUpdated=new Supply();
        try {
            //update new values
            supplyRepository.updateSupply(supply.getId(), supply.getQuantity(), supply.getPrice());

          //Converting the Object to JSONString
            ObjectMapper mapper = new ObjectMapper();
           SupUpdated= supplyRepository.findById(supply.getId()).get();
         String jsonString = mapper.writeValueAsString(SupUpdated);
         System.out.println(jsonString);
         send.sendingMessage(jsonString);
        }catch(Exception jpex){
            System.out.println(jpex);
        }
        return SupUpdated;
    }

//    @RequestMapping( method = RequestMethod.POST, value = "/update")
//    @ResponseStatus(HttpStatus.OK)
//    public Supply updateSupply(@RequestBody Supply s) {
//        Supply SupUpdated=new Supply();
//        System.out.println("PROVA");
//       //this.supTran.SaveSupplyAfterOrder(s.getId(),s.getQuantity());
//        supplyRepository.updateSupply( s.getId(), s.getQuantity(), s.getPrice());
//     try {
//            //Creating the ObjectMapper object
//        ObjectMapper mapper = new ObjectMapper();
//            //Converting the Object to JSONString
//         SupUpdated= supplyRepository.findById(s.getId()).get();
//         String jsonString = mapper.writeValueAsString(SupUpdated);
//         System.out.println(jsonString);
//         send.sendingMessage(jsonString);
//        // System.out.println("sono il reciever invio il mex");
//        }catch(JsonProcessingException jpex){
//            System.out.println(jpex);
//        }
//       return SupUpdated;
//    }

    @RequestMapping(value="/remove", method= RequestMethod.DELETE)
    public void removeSupply(@RequestBody Long supplyid) {
        supplyRepository.deleteById(supplyid);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/findShopByCategory")
    public List<Integer> findByCategory(@RequestBody String category){
        System.out.println("sono arrivato al backend");
        return supplyRepository.findShopByCategory(category);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/findSupplyById")
    public Supply findByCategory(@RequestBody Long supplyId){
        return supplyRepository.findById(supplyId).get();
    }

}
