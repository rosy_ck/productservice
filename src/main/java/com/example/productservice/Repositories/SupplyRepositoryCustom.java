package com.example.productservice.Repositories;

import com.example.productservice.Entities.Supply;
import org.springframework.data.jpa.repository.Lock;

import javax.persistence.LockModeType;
import javax.transaction.Transactional;

public interface SupplyRepositoryCustom {
    @Transactional
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    boolean SaveSupplyAfterOrder( Long supplyId,Double quantity);
}
