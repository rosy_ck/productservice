package com.example.productservice.Repositories;

import com.example.productservice.Entities.Supply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface SupplyRepository extends JpaRepository<Supply, Long> {
    @Query("SELECT s FROM Supply s WHERE s.shopId = ?1")
    List<Supply> findByShop(Long shopid);

  //  @Query("SELECT s FROM Supply s WHERE s.id = ?1")
   // Supply findById(Long supplyId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Supply s set s.quantity = ?2, s.price=?3 where s.id = ?1")
    int updateSupply( Long id, Double quantity, Double price);

    @Query("select distinct s.shopId from Supply s inner join Product p on s.product.id=p.id where p.category=?1 ")
    List<Integer> findShopByCategory(String category);
}
