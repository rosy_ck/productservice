FROM openjdk:15-alpine
RUN addgroup -S spring && adduser -S spring -G spring
VOLUME /tmp
EXPOSE 8083
ARG DEPENDENCY=target
ADD ${DEPENDENCY}/*.jar productservice-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/productservice-0.0.1-SNAPSHOT.jar"]
CMD ["productserviceapp", "serve", "--host", "0.0.0.0"]
